﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;

using MahApps.Metro.Controls;
using MahApps.Metro;

using MySql.Data.MySqlClient;

namespace MsaApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Views.SettingView settingView;
        private Views.PhaseView phaseView;
        private Dictionary<int, List<string>> TeamLists = null;
        private bool isFinishedDraw = false;
        private static int currentStep = 1;
        public int TrmId { get; set; }
        private List<List<object>> TL = null;
        private MySqlConnection connection;

        private Models.Team TeamManager;
        private Models.Tournament TournamentManager;
        private Models.Match MatchManager;
        private Models.Group GroupManager;

        public MainWindow()
        {
            InitializeComponent();

            connection = Classes.DbFactory.GetConnection();
            TeamManager = new Models.Team(connection);
            TournamentManager = new Models.Tournament(connection);
            MatchManager = new Models.Match(connection);
        }

        public MainWindow(int trmId): this()
        {
            TrmId = trmId;
            List<object> trm = TournamentManager.Get(TrmId);
            string trmName = "";

            if (trm != null) trmName = trm.ElementAt(1).ToString();
            TrnLabel.Text = string.Format(trmName.ToUpper());

            if (TournamentManager.IsSelected(TrmId))
            {
                LoadTeamInfoFromDb();
            }
            else
            {
                LoadTeamInfoFromFile();
            }            
        }
                
        /// <summary>
        /// Key Events
        /// </summary>
        private void App_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Escape: ExitFullScreen();
                    break;
                case Key.F11: SetFullScreen();
                    break;
                case Key.F12: OpenSettings();
                    break;
                case Key.Delete: QuitMainApplication();
                    break;
                
                /// Teams key events
                /// 

                case Key.NumPad0: 
                    if (!TournamentManager.IsSelected(TrmId))
                    {
                        ResetTeams();
                        isFinishedDraw = false;
                        currentStep = 1;
                    }
                    break;
                case Key.Home:
                    if (!TournamentManager.IsSelected(TrmId))
                    {
                        ResetTeams();
                        //Team1.Text = "Loading...";
                        isFinishedDraw = false;
                        currentStep = 1;
                    }
                    break;

                case Key.NumPad1:
                    if (!TournamentManager.IsSelected(TrmId))
                    {
                        GetTeamInfo(1);
                    }                    
                    break;
                case Key.NumPad2:
                    if (!TournamentManager.IsSelected(TrmId))
                    {
                        GetTeamInfo(2);
                    }
                    break;
                case Key.NumPad3:
                    if (!TournamentManager.IsSelected(TrmId))
                    {
                        GetTeamInfo(3);
                    }
                    break;
                case Key.NumPad4:
                    if (!TournamentManager.IsSelected(TrmId))
                    {
                        GetTeamInfo(4);
                    }
                    break;
                case Key.NumPad5:
                    if (!TournamentManager.IsSelected(TrmId))
                    {
                        GetTeamInfo(5);
                    }
                    break;
                case Key.NumPad6:
                    if (!TournamentManager.IsSelected(TrmId))
                    {
                        GetTeamInfo(6);
                    }
                    break;
                case Key.NumPad7:
                    if (!TournamentManager.IsSelected(TrmId))
                    {
                        GetTeamInfo(7);
                    }
                    break;
                case Key.NumPad8:
                    if (!TournamentManager.IsSelected(TrmId))
                    {
                        GetTeamInfo(8);
                    }
                    break;
                case Key.NumPad9:
                    if (!TournamentManager.IsSelected(TrmId))
                    {
                        GetTeamInfo(9);
                    }
                    break;
                case Key.Q:
                    if (!TournamentManager.IsSelected(TrmId))
                    {
                        GetTeamInfo(10);
                    }
                    break;
                case Key.S:
                    if (!TournamentManager.IsSelected(TrmId))
                    {
                        GetTeamInfo(11);
                    }
                    break;
                case Key.D:
                    if (!TournamentManager.IsSelected(TrmId))
                    {
                        GetTeamInfo(12);
                    }
                    break;

                /// Phase de poule
                /// 
                case Key.P: phaseView = new Views.PhaseView(TrmId);
                    phaseView.ShowDialog();
                    break;
            }
        }

        /// <summary>
        /// Click Events
        /// </summary>

        /// Custom Methods
        /// 

        private void LoadTeamInfoFromDb()
        {
            TL = TeamManager.GetTeamsFromIdTrm(TrmId);

            if (TL != null)
            {
                List<TextBlock> listTextBlockTeams = new List<TextBlock>()
                {
                    Team1, Team2, Team3, Team4, Team5, Team6, Team7, Team8, Team9, Team10, Team11, Team12
                };
                List<Image> listImageTeams = new List<Image>()
                {
                    Team1Logo, Team2Logo, Team3Logo, Team4Logo, Team5Logo, Team6Logo, 
                    Team7Logo, Team8Logo, Team9Logo, Team10Logo, Team11Logo, Team12Logo
                };

                for (int i = 0; i < TL.Count; i++)
                {
                    listTextBlockTeams[i].Text = TL[i].ElementAt(1).ToString();
                }
            }
        }

        private void LoadTeamInfoFromFile()
        {
            string filepath = System.IO.Path.GetFullPath("../../Files/Teams.csv");

            TL = TeamManager.GetFromIdTrm(TrmId);

            if (TL != null)
            {
                int index = 0;
                List<string> listTeam = new List<string>();
                string line = "";

                foreach (var item in TL)
                {
                    line = string.Format("{0};{1};{2};{3}", ++index, item.ElementAt(1), item.ElementAt(2), item.ElementAt(0));
                    listTeam.Add(line);
                }
                File.WriteAllLines(filepath, listTeam);
            }

            string[] teams = File.ReadAllLines(filepath);
            InitDico(teams);         
        }

        private void InitDico(string[] teams)
        {
            TeamLists = new Dictionary<int, List<string>>();
            List<string> listString = null;

            for (int i = 0; i < teams.Length; i++)
            {
                string currentTeam = teams[i];
                string[] parsedTeamInfo = ParseCSV(currentTeam);
                listString = new List<string>();
                listString.Add(parsedTeamInfo[1]);
                listString.Add(parsedTeamInfo[2]);
                listString.Add(parsedTeamInfo[3]);
                TeamLists.Add(int.Parse(parsedTeamInfo[0]), listString);
            }           
        }

        private string[] ParseCSV(string line)
        {
            string[] team = line.Split(';');
            return team;
        }

        private void GetTeamInfo(int teamIdInFile)
        {
            List<string> listValueForOneTeam = null;            
            if (TeamLists.TryGetValue(teamIdInFile, out listValueForOneTeam))
            {
                string team = listValueForOneTeam.ElementAt(0);
                string flag = listValueForOneTeam.ElementAt(1);
                int teamIdInDb = int.Parse(listValueForOneTeam.ElementAt(2));

                if (!isFinishedDraw)
                {
                    switch(currentStep)
                    {
                        case 1: Team1.Text = string.Format("{0}", team);
                            Team1Logo.Source = new BitmapImage(new Uri("/Images/flags/" + flag, UriKind.Relative));
                            // Team2.Text = string.Format("Loading...");
                            TeamManager.UpdateNumGroup(teamIdInDb, currentStep, 1);
                            currentStep++;
                            break;
                        case 2: Team2.Text = string.Format("{0}", team);
                            Team2Logo.Source = new BitmapImage(new Uri("/Images/flags/" + flag, UriKind.Relative));
                            // Team3.Text = string.Format("Loading...");
                            TeamManager.UpdateNumGroup(teamIdInDb, currentStep, 1);
                            currentStep++;
                            break;
                        case 3: Team3.Text = string.Format("{0}", team);
                            Team3Logo.Source = new BitmapImage(new Uri("/Images/flags/" + flag, UriKind.Relative));
                            // Team4.Text = string.Format("Loading...");
                            TeamManager.UpdateNumGroup(teamIdInDb, currentStep, 1);
                            currentStep++;
                            break;
                        case 4: Team4.Text = string.Format("{0}", team);
                            Team4Logo.Source = new BitmapImage(new Uri("/Images/flags/" + flag, UriKind.Relative));
                            // Team5.Text = string.Format("Loading...");
                            TeamManager.UpdateNumGroup(teamIdInDb, currentStep, 2);
                            currentStep++;
                            break;
                        case 5: Team5.Text = string.Format("{0}", team);
                            Team5Logo.Source = new BitmapImage(new Uri("/Images/flags/" + flag, UriKind.Relative));
                            // Team6.Text = string.Format("Loading...");
                            TeamManager.UpdateNumGroup(teamIdInDb, currentStep, 2);
                            currentStep++;
                            break;
                        case 6: Team6.Text = string.Format("{0}", team);
                            Team6Logo.Source = new BitmapImage(new Uri("/Images/flags/" + flag, UriKind.Relative));
                            // Team7.Text = string.Format("Loading...");
                            TeamManager.UpdateNumGroup(teamIdInDb, currentStep, 2);
                            currentStep++;
                            break;
                        case 7: Team7.Text = string.Format("{0}", team);
                            Team7Logo.Source = new BitmapImage(new Uri("/Images/flags/" + flag, UriKind.Relative));
                            // Team8.Text = string.Format("Loading...");
                            TeamManager.UpdateNumGroup(teamIdInDb, currentStep, 3);
                            currentStep++;
                            break;
                        case 8: Team8.Text = string.Format("{0}", team);
                            Team8Logo.Source = new BitmapImage(new Uri("/Images/flags/" + flag, UriKind.Relative));
                            // Team9.Text = string.Format("Loading...");
                            TeamManager.UpdateNumGroup(teamIdInDb, currentStep, 3);
                            currentStep++;
                            break;
                        case 9: Team9.Text = string.Format("{0}", team);
                            Team9Logo.Source = new BitmapImage(new Uri("/Images/flags/" + flag, UriKind.Relative));
                            // Team10.Text = string.Format("Loading...");
                            TeamManager.UpdateNumGroup(teamIdInDb, currentStep, 3);
                            currentStep++;
                            break;
                        case 10: Team10.Text = string.Format("{0}", team);
                            Team10Logo.Source = new BitmapImage(new Uri("/Images/flags/" + flag, UriKind.Relative));
                            // Team11.Text = string.Format("Loading...");
                            TeamManager.UpdateNumGroup(teamIdInDb, currentStep, 4);
                            currentStep++;
                            break;
                        case 11: Team11.Text = string.Format("{0}", team);
                            Team11Logo.Source = new BitmapImage(new Uri("/Images/flags/" + flag, UriKind.Relative));
                            // Team12.Text = string.Format("Loading...");
                            TeamManager.UpdateNumGroup(teamIdInDb, currentStep, 4);
                            currentStep++;
                            break;
                        case 12: Team12.Text = string.Format("{0}", team);
                            Team12Logo.Source = new BitmapImage(new Uri("/Images/flags/" + flag, UriKind.Relative));
                            TeamManager.UpdateNumGroup(teamIdInDb, currentStep, 4);
                            isFinishedDraw = true;
                            TournamentManager.UpdateSelected(TrmId);
                            GenerateFirstMatch();
                            break;
                    }                    
                }
                
                // string imageSource = System.IO.Path.GetFullPath("Images/flags/bra.png");
                // Team1Logo.Source = new BitmapImage(new Uri(imageSource, UriKind.Relative)); 
            }
        }

        private void ResetTeams()
        {
            int index = 0;
            List<TextBlock> listTextBlockTeams = new List<TextBlock>()
            {
                Team1, Team2, Team3, Team4, Team5, Team6, Team7, Team8, Team9, Team10, Team11, Team12
            };
            List<Image> listImageTeams = new List<Image>()
            {
                Team1Logo, Team2Logo, Team3Logo, Team4Logo, Team5Logo, Team6Logo, 
                Team7Logo, Team8Logo, Team9Logo, Team10Logo, Team11Logo, Team12Logo
            };

            foreach (var team in listTextBlockTeams)
            {
                team.Text = string.Format("Team {0}", ++index);
            }
            foreach (var team in listImageTeams)
            {
                team.Source = new BitmapImage(new Uri("/Images/flags/default.png", UriKind.Relative));
            }
        }

        private void GenerateFirstMatch()
        {
            string  label = "Phase de poule",
                    date_match = "0000-00-00";
            int score_first_team = 0,
                score_second_team = 0,
                id_first_team = 0,
                id_second_team = 0,
                id_tournament = TrmId,
                id_step = 1;

            List<object> match = null;
            List<List<object>> groupA = new List<List<object>>();
            List<List<object>> groupB = new List<List<object>>();
            List<List<object>> groupC = new List<List<object>>();
            List<List<object>> groupD = new List<List<object>>();

            List<List<List<object>>> listGroup = new List<List<List<object>>>()
            {
                groupA, groupB, groupC, groupD
            };

            TL = TeamManager.GetTeamsFromIdTrm(TrmId);

            if (TL != null)
            {
                foreach (var team in TL)
                {
                    switch (int.Parse(team.ElementAt(5).ToString()))
                    {
                        case 1: groupA.Add(team);
                            break;
                        case 2: groupB.Add(team);
                            break;
                        case 3: groupC.Add(team);
                            break;
                        case 4: groupD.Add(team);
                            break;
                    }
                }
            }
            
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    match = new List<object>();

                    match.Add(label);
                    match.Add(date_match);
                    match.Add(score_first_team);
                    match.Add(score_second_team);

                    if (j == 0)
                    {
                        id_first_team = int.Parse(listGroup.ElementAt(i).ElementAt(j).ElementAt(0).ToString());
                        id_second_team = int.Parse(listGroup.ElementAt(i).ElementAt(j + 1).ElementAt(0).ToString());
                    }
                    else if (j == 1)
                    {
                        id_first_team = int.Parse(listGroup.ElementAt(i).ElementAt(j-1).ElementAt(0).ToString());
                        id_second_team = int.Parse(listGroup.ElementAt(i).ElementAt(j+1).ElementAt(0).ToString());
                    }
                    else if (j == 2)
                    {
                        id_first_team = int.Parse(listGroup.ElementAt(i).ElementAt(j-1).ElementAt(0).ToString());
                        id_second_team = int.Parse(listGroup.ElementAt(i).ElementAt(j).ElementAt(0).ToString());
                    }

                    match.Add(id_first_team);
                    match.Add(id_second_team);
                    match.Add(id_tournament);
                    match.Add(id_step);
                    match.Add(int.Parse(listGroup.ElementAt(i).ElementAt(j).ElementAt(5).ToString()));

                    MatchManager.Add(match);
                }
            }

        }

        private void ExitFullScreen()
        {
            if (this.WindowStyle.Equals(WindowStyle.None))
                this.WindowStyle = WindowStyle.SingleBorderWindow;
        }

        private void SetFullScreen()
        {
            if (!this.WindowStyle.Equals(WindowStyle.None))
            {
                this.WindowState = WindowState.Normal;
                this.WindowStyle = WindowStyle.None;
                this.WindowState = WindowState.Maximized;
            }
        }

        private void QuitApplication()
        {
            Environment.Exit(0);
        }

        private void QuitMainApplication()
        {
            Views.StatView startView = new Views.StatView();
            startView.Show();
            this.Hide();
        }

        private void OpenSettings()
        {
            settingView = new Views.SettingView(TrmId);
            settingView.ShowDialog();
        }

    }
}
