﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace MsaApp.Views
{
    /// <summary>
    /// Interaction logic for TimingView.xaml
    /// </summary>
    public partial class TimingView : Window
    {
        private DispatcherTimer timer;

        public TimingView()
        {
            InitializeComponent();
            
            timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Tick += timer_Tick; 
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            int min = int.Parse(TmMinute.Text);
            int sec = int.Parse(TmSecond.Text);

            if (sec < 59)
            {
                sec++;
            }
            else
            {
                min += 1;
                sec = 0;
            }

            TmSecond.Text = (sec <= 9) ? "0" + sec : "" + sec;
            TmMinute.Text = (min <= 9) ? "0" + min : "" + min;
        }
        
        private void TimingWindow_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Delete: QuitApp();
                    break;
                case Key.NumPad4: UpTeam1Score();
                    break;
                case Key.NumPad1: DownTeam1Score();
                    break;
                case Key.NumPad6: UpTeam2Score();
                    break;
                case Key.NumPad3: DownTeam2Score();
                    break;
                case Key.Space: PauseTiming();
                    break;
                case Key.Enter: StartTiming();
                    break;
            }
        }

        private void UpTeam1Score()
        {
            int score = int.Parse(Team1Score.Text);
            score++;
            Team1Score.Text = string.Format("{0}", score);
        }

        private void DownTeam1Score()
        {
            int score = int.Parse(Team1Score.Text);
            if (score > 0) score--;
            Team1Score.Text = string.Format("{0}", score);
        }

        private void UpTeam2Score()
        {
            int score = int.Parse(Team2Score.Text);
            score++;
            Team2Score.Text = string.Format("{0}", score);
        }

        private void DownTeam2Score()
        {
            int score = int.Parse(Team2Score.Text);
            if (score > 0) score--;
            Team2Score.Text = string.Format("{0}", score);
        }

        private void PauseTiming()
        {
            if (timer.IsEnabled == true) timer.IsEnabled = false;
            else if (timer.IsEnabled == false) timer.IsEnabled = true;
        }

        private void StartTiming()
        {
            if (!timer.IsEnabled)
                timer.Start();
        }

        private void TimingWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            QuitApp();
        }

        private void QuitApp()
        {
            //this.Hide();
            Environment.Exit(0);
        }
    }
}
