﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MySql.Data.MySqlClient;
using System.Windows;

namespace MsaApp.Models
{
    class Tournament: Interface.IManager
    {
        private MySqlConnection connection;

        public Tournament(MySqlConnection connection) 
        {
            this.connection = connection;
        }

        public bool Add(List<object> team)
        {
            // name, date_tournament, date_add
            string query = "INSERT INTO tournaments(name, date_tournament, date_add) " +
                            "VALUES(@name, @date_tournament, NOW())";

            if (Classes.DbFactory.OpenConnection())
            {
                MySqlCommand command = new MySqlCommand(query, connection);
                command.Parameters.AddWithValue("name", team.ElementAt(0));
                command.Parameters.AddWithValue("date_tournament", team.ElementAt(1));

                command.ExecuteNonQuery();
                Classes.DbFactory.CloseConnection();
                return true;
            }
            return false;
        }

        public List<object> Get(int id)
        {
            List<object> tournament = new List<object>();
            string query = "SELECT id, name, date_tournament FROM tournaments WHERE id = @id";

            if (Classes.DbFactory.OpenConnection())
            {
                MySqlCommand command = new MySqlCommand(query, connection);
                command.Parameters.AddWithValue("id", id);
                MySqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    reader.Read();
                    tournament.Add(reader["id"]);
                    tournament.Add(reader["name"]);
                    tournament.Add(reader["date_tournament"]);                    
                }
                reader.Close();
                Classes.DbFactory.CloseConnection();
                return tournament;
            }
            return null;
        }

        public List<List<object>> GetAll()
        {
            List<List<object>> listTournament = new List<List<object>>();
            string query = "SELECT id, name, date_tournament FROM tournaments ORDER BY date_add";

            if (Classes.DbFactory.OpenConnection())
            {
                MySqlCommand command = new MySqlCommand(query, connection);
                MySqlDataReader reader = command.ExecuteReader();

                List<object> item;
                while (reader.Read())
                {
                    item = new List<object>();
                    item.Add(reader["id"]);
                    item.Add(reader["name"]);
                    item.Add(reader["date_tournament"]);
                    listTournament.Add(item);
                }
                reader.Close();
                Classes.DbFactory.CloseConnection();
                return listTournament;
            }
            return null;
        }

        public bool IsSelected(int idTrm)
        {
            int selectedField = 0;
            string query = "SELECT selected FROM tournaments WHERE id = @id";

            if (Classes.DbFactory.OpenConnection())
            {
                MySqlCommand command = new MySqlCommand(query, connection);
                command.Parameters.AddWithValue("id", idTrm);
                MySqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    reader.Read();
                    selectedField = int.Parse(reader["selected"].ToString());
                }
                reader.Close();
                Classes.DbFactory.CloseConnection();
                return (selectedField == 0) ? false : true;
            }
            return false;
        }

        public bool Update(int teamId, List<object> team)
        {
            return false;
        }

        public bool UpdateSelected(int trmId)
        {
            string query = "UPDATE tournaments SET selected=1 WHERE id = @id";

            if (Classes.DbFactory.OpenConnection())
            {
                MySqlCommand command = new MySqlCommand(query, connection);
                command.Parameters.AddWithValue("id", trmId);
                command.ExecuteNonQuery();
                Classes.DbFactory.CloseConnection();
                return true;
            }
            return false;
        }

        public bool Delete(int teamId)
        {
            throw new NotImplementedException();
        }

        public int Count()
        {
            throw new NotImplementedException();
        }
    }
}
