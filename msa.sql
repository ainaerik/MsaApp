-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  mar. 19 déc. 2017 à 14:06
-- Version du serveur :  10.1.28-MariaDB
-- Version de PHP :  5.6.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `msa`
--

-- --------------------------------------------------------

--
-- Structure de la table `groups`
--

CREATE TABLE `groups` (
  `id` int(11) NOT NULL,
  `name` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `groups`
--

INSERT INTO `groups` (`id`, `name`) VALUES
(1, 'A'),
(2, 'B'),
(3, 'C'),
(4, 'D'),
(5, 'NONE');

-- --------------------------------------------------------

--
-- Structure de la table `matchs`
--

CREATE TABLE `matchs` (
  `id` int(11) NOT NULL,
  `label` varchar(5) NOT NULL,
  `date_match` datetime NOT NULL,
  `score_first_team` int(11) NOT NULL,
  `score_second_team` int(11) NOT NULL,
  `id_first_team` int(11) NOT NULL,
  `id_second_team` int(11) NOT NULL,
  `id_tournament` int(11) NOT NULL,
  `id_step` int(11) NOT NULL,
  `id_group` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `matchs`
--

INSERT INTO `matchs` (`id`, `label`, `date_match`, `score_first_team`, `score_second_team`, `id_first_team`, `id_second_team`, `id_tournament`, `id_step`, `id_group`) VALUES
(13, 'Phase', '0000-00-00 00:00:00', 0, 0, 11, 18, 8, 1, 1),
(14, 'Phase', '0000-00-00 00:00:00', 0, 0, 11, 22, 8, 1, 1),
(15, 'Phase', '0000-00-00 00:00:00', 0, 0, 18, 22, 8, 1, 1),
(16, 'Phase', '0000-00-00 00:00:00', 0, 0, 17, 21, 8, 1, 2),
(17, 'Phase', '0000-00-00 00:00:00', 0, 0, 17, 24, 8, 1, 2),
(18, 'Phase', '0000-00-00 00:00:00', 0, 0, 21, 24, 8, 1, 2),
(19, 'Phase', '0000-00-00 00:00:00', 0, 0, 16, 19, 8, 1, 3),
(20, 'Phase', '0000-00-00 00:00:00', 0, 0, 16, 23, 8, 1, 3),
(21, 'Phase', '0000-00-00 00:00:00', 0, 0, 19, 23, 8, 1, 3),
(22, 'Phase', '0000-00-00 00:00:00', 0, 0, 25, 26, 8, 1, 4),
(23, 'Phase', '0000-00-00 00:00:00', 0, 0, 25, 27, 8, 1, 4),
(24, 'Phase', '0000-00-00 00:00:00', 0, 0, 26, 27, 8, 1, 4);

-- --------------------------------------------------------

--
-- Structure de la table `steps`
--

CREATE TABLE `steps` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `steps`
--

INSERT INTO `steps` (`id`, `name`) VALUES
(1, 'Phase de poule'),
(2, 'Huitième de final'),
(3, 'Quart de final'),
(4, 'Demi final'),
(5, 'Final');

-- --------------------------------------------------------

--
-- Structure de la table `teams`
--

CREATE TABLE `teams` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `logo` text NOT NULL,
  `numero` int(11) NOT NULL,
  `date_add` datetime NOT NULL,
  `id_groupe` int(11) NOT NULL,
  `id_tournament` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `teams`
--

INSERT INTO `teams` (`id`, `name`, `logo`, `numero`, `date_add`, `id_groupe`, `id_tournament`) VALUES
(4, 'sdf', 'default.png', 1, '2017-12-19 10:29:10', 1, 1),
(5, 'msa', 'default.png', 12, '2017-12-19 10:29:35', 4, 1),
(6, 'poer', 'default.png', 11, '2017-12-19 10:29:41', 4, 1),
(7, 'azert', 'default.png', 10, '2017-12-19 10:31:14', 4, 1),
(11, 'ertert', 'default.png', 1, '2017-12-19 11:50:09', 1, 8),
(12, 'az', 'default.png', 8, '2017-12-19 11:58:15', 3, 1),
(13, 'as', 'default.png', 6, '2017-12-19 11:58:17', 2, 1),
(14, 'aq', 'default.png', 9, '2017-12-19 11:58:20', 3, 1),
(15, 'ax', 'default.png', 7, '2017-12-19 11:58:23', 3, 1),
(16, 'wx', 'default.png', 7, '2017-12-19 11:58:52', 3, 8),
(17, 'wv', 'default.png', 6, '2017-12-19 11:58:55', 2, 8),
(18, 'wo', 'default.png', 2, '2017-12-19 11:58:58', 1, 8),
(19, 'wp', 'default.png', 8, '2017-12-19 11:59:01', 3, 8),
(20, 'dsfdsf', 'default.png', 5, '2017-12-19 12:12:53', 2, 1),
(21, 'lj', 'default.png', 5, '2017-12-19 12:37:22', 2, 8),
(22, 'lm', 'default.png', 3, '2017-12-19 12:37:26', 1, 8),
(23, 'mp', 'default.png', 9, '2017-12-19 12:37:28', 3, 8),
(24, 'mr', 'default.png', 4, '2017-12-19 12:37:31', 2, 8),
(25, 'lv', 'default.png', 12, '2017-12-19 12:37:35', 4, 8),
(26, 'ln', 'default.png', 11, '2017-12-19 12:37:38', 4, 8),
(27, 'la', 'default.png', 10, '2017-12-19 12:37:43', 4, 8),
(28, 'xs', 'default.png', 4, '2017-12-19 14:06:01', 2, 1),
(29, 'xc', 'default.png', 3, '2017-12-19 14:06:04', 1, 1),
(30, 'xv', 'default.png', 2, '2017-12-19 14:06:08', 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `tournaments`
--

CREATE TABLE `tournaments` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `date_tournament` datetime NOT NULL,
  `date_add` datetime NOT NULL,
  `selected` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `tournaments`
--

INSERT INTO `tournaments` (`id`, `name`, `date_tournament`, `date_add`, `selected`) VALUES
(1, 'AZ', '2017-12-23 00:00:00', '2017-12-19 10:28:12', 1),
(8, 'mondial', '2017-12-23 00:00:00', '2017-12-19 11:30:47', 1),
(9, 'azerty', '2017-12-30 00:00:00', '2017-12-19 12:25:57', 0);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `matchs`
--
ALTER TABLE `matchs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_step` (`id_step`),
  ADD KEY `id_first_team` (`id_first_team`,`id_second_team`,`id_tournament`),
  ADD KEY `id_second_team` (`id_second_team`),
  ADD KEY `id_tournament` (`id_tournament`),
  ADD KEY `id_group` (`id_group`);

--
-- Index pour la table `steps`
--
ALTER TABLE `steps`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `teams`
--
ALTER TABLE `teams`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_groupe` (`id_groupe`,`id_tournament`),
  ADD KEY `id_tournament` (`id_tournament`);

--
-- Index pour la table `tournaments`
--
ALTER TABLE `tournaments`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `matchs`
--
ALTER TABLE `matchs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT pour la table `steps`
--
ALTER TABLE `steps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `teams`
--
ALTER TABLE `teams`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT pour la table `tournaments`
--
ALTER TABLE `tournaments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `matchs`
--
ALTER TABLE `matchs`
  ADD CONSTRAINT `matchs_ibfk_1` FOREIGN KEY (`id_first_team`) REFERENCES `teams` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `matchs_ibfk_2` FOREIGN KEY (`id_second_team`) REFERENCES `teams` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `matchs_ibfk_3` FOREIGN KEY (`id_tournament`) REFERENCES `tournaments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `matchs_ibfk_4` FOREIGN KEY (`id_step`) REFERENCES `steps` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `matchs_ibfk_5` FOREIGN KEY (`id_group`) REFERENCES `groups` (`id`);

--
-- Contraintes pour la table `teams`
--
ALTER TABLE `teams`
  ADD CONSTRAINT `teams_ibfk_1` FOREIGN KEY (`id_groupe`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `teams_ibfk_2` FOREIGN KEY (`id_tournament`) REFERENCES `tournaments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
